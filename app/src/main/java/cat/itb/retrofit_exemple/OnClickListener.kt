package cat.itb.retrofit_exemple

import cat.itb.retrofit_exemple.model.Character

interface OnClickListener {
    fun onClick(character: Character)
}