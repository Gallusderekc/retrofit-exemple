package cat.itb.retrofit_exemple.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cat.itb.retrofit_exemple.OnClickListener
import cat.itb.retrofit_exemple.R
import cat.itb.retrofit_exemple.databinding.ItemCharacterGridBinding
import cat.itb.retrofit_exemple.databinding.ItemCharacterListBinding
import cat.itb.retrofit_exemple.model.Character


class CharacterAdapter(private val characters: List<Character>, private val listener: OnClickListener): RecyclerView.Adapter<CharacterAdapter.ViewHolder>() {

    private lateinit var context: Context
    private var layoutType = 0 //0 - LinearLayout, 1 - GridLayout

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = if(layoutType == 0) ItemCharacterListBinding.bind(view) else ItemCharacterGridBinding.bind(view)
        fun setListener(character: Character){
            binding.root.setOnClickListener {
                listener.onClick(character)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        var layoutId: Int
        if(layoutType == 0){
            layoutId = R.layout.item_character_list
        }
        else{
            layoutId = R.layout.item_character_grid
        }
        val view = LayoutInflater.from(context).inflate(layoutId, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val character = characters[position]
        with(holder){
            setListener(characters[position])
            if(binding is ItemCharacterListBinding){
                binding.tvName.text = character.name
                binding.tvHeight.text = "Height: ${character.height}"
                binding.tvMass.text = "Mass: ${character.mass}"
            }
            else{
                (binding as ItemCharacterGridBinding)
                binding.tvName.text = character.name
                binding.tvHeight.text = "Height: ${character.height}"
                binding.tvMass.text = "Mass: ${character.mass}"
            }
        }
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    fun setLayoutType(type: Int){
        layoutType = type
    }

}