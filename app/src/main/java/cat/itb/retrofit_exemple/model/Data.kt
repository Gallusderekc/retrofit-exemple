package cat.itb.retrofit_exemple.model

data class Data(
    val count: Int,
    val next: String,
    val previous: String,
    val results: List<Character>
)