package cat.itb.retrofit_exemple.retrofit


import cat.itb.retrofit_exemple.model.Data
import cat.itb.retrofit_exemple.model.Planet
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiInterface {

    @GET()
    fun getData(@Url url: String): retrofit2.Call<Data>

    @GET()
    fun getPlanet(@Url url: String): retrofit2.Call<Planet>

    companion object {
        val BASE_URL = "https://swapi.dev/api/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}