package cat.itb.retrofit_exemple.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cat.itb.retrofit_exemple.model.Character
import cat.itb.retrofit_exemple.Repository
import cat.itb.retrofit_exemple.model.Data
import cat.itb.retrofit_exemple.model.Planet

class MyViewModel: ViewModel() {
    val repository = Repository()
    var data = MutableLiveData<Data>()
    var actualCharacter = MutableLiveData<Character>()
    var planetData = MutableLiveData<Planet>()

    init {
        fetchData("people")
    }

    fun fetchData(url: String){
        data = repository.fetchData(url)
    }

    fun setCharacter(character: Character){
        planetData = repository.getPlanetData(character.homeworld)
        actualCharacter.value = character
    }
}